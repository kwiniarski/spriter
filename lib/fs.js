var fs = require('fs');
var path = require('path');
var async = require('async');

function read(baseDir, filter, callback) {
	var list = [];
//	var fileType = false;
//	var re = false;
	var q = async.queue(function(dir, fn){
		fs.readdir(dir, function(err, files){
			fn(err, files, dir);
		});
	}, 10);

//	if (pattern instanceof Array) {
//		re = new RegExp('\\.(' + pattern.join('|') + ')$', 'i');
//		fileType = function (file) {
//			return re.test(file);
//		}
//	}
	if (!callback && typeof filter === 'function') {
		callback = filter;
	}

	function stat(file, fn) {
		var name = path.join(this.dir, file);
		fs.stat(name, function (err, stats) {
			stats.path = name;
			if (stats.isDirectory()) {
				q.push(name, process);
			}
			fn(err, stats);
		});
	}

	function process(err, files, dir) {
		if (err || !files || !files.length) {
			return;
		}
//		var relDir = path.relative(baseDir, dir);
		async.map(files, stat.bind({
			dir: dir
		}), function (err, result) {
//			console.log(files, result);
			list = list.concat(result);
		});
//		files.forEach(function (file) {
//			if (!fileType || fileType(file)) {
//				list.push(path.join(relDir, file));
//			} else {
//				q.push(path.join(dir, file), process);
//			}
//		});
	}

	q.drain = function(){
		callback(list);
	};

	q.push(baseDir, process);
}

function mk(dir) {
	var absDir = path.resolve(process.cwd(), dir);
	var dirs = absDir.split(path.sep);
	var mount = dirs.shift() + path.sep;

	while (dirs.length) {
		mount = path.join(mount, dirs.shift());
		if (!fs.existsSync(mount)) {
			try {
				fs.mkdirSync(mount);
			} catch (e) {
				return false;
			}
		}
	}
	return true;
}

function rm(dir) {
	var absDir = path.resolve(process.cwd(), dir);
//	var dirs = absDir.split(path.sep);
//	var mount = dirs.shift() + path.sep;
	//fs.rmdirSync(absDir);
	read(absDir, function(files){
		console.log(1, files);
	});
}

//mk('../test2/lib/some/dir');
rm('../test');


//scan('../test/', ['png'], function(files) {
//	console.log(files);
//});
