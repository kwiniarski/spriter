var glob = require('glob');
var gm = require('gm');
var fs = require('fs');
var path = require('path');
var async = require('async');
var util = require('util');
var events = require('events');
var _ = require('lodash');
//var SpriteConfig = require('./config.js');
var SpriteImage = require('./image.js');

function getFiles(dir, fn) {
	glob('**/*.{png,gif}', {
		cwd: dir
	}, function (err, matches) {
		if (err) {
			return fn(err);
		}
		fn(null, matches.map(function (res) {
			return path.join(dir, res);
		}));
	});
}

//function writeData(map, next) {
//	var emitter = this.emitter(this.name, next),
//		writer = this.writter,
//		template = this.template,
//		file = this.file;
//
//	fs.exists(template, function (exists) {
//		if (exists) {
//			fs.readFile(template, function (err, tpl) {
//				if (err) {
//					return next(err);
//				}
//				writer(file, _.template(tpl, map), emitter);
//			});
//		} else {
//			writer(file, map, emitter);
//		}
//	});
//}

function genericWriter(file, data, fn) {
	fs.writeFile(file, data, function(err) {
		fn(err, data);
	});
}

function SpriteMap(name, options) {

	events.EventEmitter.call(this);

	if (this instanceof SpriteMap === false) {
		return new SpriteMap(name, options);
	}

	if (typeof name === 'undefined' || !/^[a-z][\w\-]*[a-z0-9]+$/i.test(name)) {
		throw new Error('Invalid sprite map name: "' + name + '".');
	}

	if (typeof options !== 'undefined') {
		this.validateOptions(options);
	}

	/**
	 * Resolved sprite map options. Properties not provided by user
	 * are replaced by default values.
	 * @type {Object}
	 */
	this.options = _.extend({}, this._defaults, options || {});

	/**
	 * Sprite map name.
	 * @type {String}
	 */
	this.name = name;

	/**
	 * Resolved src and dest paths
	 * @type {String}
	 */
	['src', 'dest'].forEach(function (i) {
		this[i] = path.resolve(this._cwd, this.options[i]);
	}.bind(this));

	/**
	 * Generated list of include and exclude paths/files
	 * @type {Array}
	 */
	['include', 'exclude'].forEach(function (i) {
		this[i] = (this.options[i] instanceof Array ? this.options[i] : [this.options[i]]).map(function (res) {
			return path.resolve(this._cwd, res);
		}.bind(this));
	}.bind(this));


	this.layout = this._layouts[this.options.layout];

	/**
	 * Object with available output generators. Generator can be defined
	 * as Node.js module (.js file) or as a lo-dash template (.tpl file).
	 *
	 */
	this.writers = _.reduce(this._writers, function (writers, writerFn, writerName) {
		if (_.contains(this.options.output, writerName)) {
			writers.push(function (map, next) {
				var file = this.getFileName(map.name, writerName),
					emitter = this.emitter(writerName, next),
					template = 'lib/writers/templates/template.' + writerName;

				fs.exists(template, function (exists) {
					if (exists) {
						fs.readFile(template, function (err, tpl) {
							if (err) {
								return next(err);
							}
							writerFn(file, _.template(tpl, map), emitter);
						});
					} else {
						writerFn(file, map, emitter);
					}
				});
			}.bind(this));
		}
		return writers;
    }, [], this);

	this._emitterData = {};

}

util.inherits(SpriteMap, events.EventEmitter);

_.assign(SpriteMap.prototype, {
	_layouts: {
		cross:		require('./layouts/cross.js'),
		vertical:	require('./layouts/vertical.js'),
		smart:		require('./layouts/smart.js')
	},
	_writers: {
		png:		require('./writers/png.js'),
		css:		genericWriter,
		json:		require('./writers/json.js'),
		html:		genericWriter,
		styl:		genericWriter,
		less:		genericWriter
	},
	/**
	 * Process cwd. Used to resolve configuration paths.
	 */
	_cwd: process.cwd(),

	/**
	 * Default configuration options.
	 */
	_defaults: {
		src: '.',
		dest: '.',
		include: [],
		exclude: [],
		output: ['png', 'css'],
		imagesDir: '.',
		imagesHost: false,
		imagesSpace: 0,
		layout: 'smart',
		optimize: false,
		merge: true
	},

	/**
	 * Method used to validate user provided options.
	 * @throws Error
	 */
	validateOptions: function (options) {
		if (!_.isPlainObject(options)) {
			throw new Error('Options should be an object.');
		}
	},

	/**
	 * Utility method to generate list of files from include or exclude option.
	 * @param property {String} Option's property with list of paths/files.
	 * @param fn {Function} Callback function that gets 2 arguments: error and callback.
	 * @returns undefined
	 */
	files: function (property, fn) {
		var isFileRx = /\.(png|gif|jpg|jpeg)$/i,
			directories = this[property].filter(function (res) {
				return !isFileRx.test(res);
			}),
			files = this[property].filter(function (res) {
				return isFileRx.test(res);
			});

		async.concat(directories, getFiles, function (err, result) {
			if (err) {
				return fn(err);
			}
			fn(null, result.concat(files));
		});
	},

	/**
	 * Gets list of files from the source directory.
	 * @param fn {Function} Callback function that gets 2 arguments: error and callback.
	 * @returns undefined
	 */
	getSourceFiles: function (fn) {
		getFiles(this.src, fn);
	},

	/**
	 * Gets list of files from the resources defined in the include option.
	 * @param fn {Function} Callback function that gets 2 arguments: error and callback.
	 * @returns undefined
	 */
	getIncludeFiles: function (fn) {
		this.files('include', fn);
	},

	/**
	 * Gets list of files from the resources defined in the exclude option.
	 * @param fn {Function} Callback function that gets 2 arguments: error and callback.
	 * @returns undefined
	 */
	getExcludeFiles: function (fn) {
		this.files('exclude', fn);
	},

	splitIntoMaps: function (files) {
		return _.groupBy(files, function (file) {
			var rel = path.relative(this.src, file),
				dir = path.dirname(rel);
			return (this.name + '-' + dir.replace(/[\\\/]/g, '-')).replace(/\-\.?$/, '');
		}, this);
	},

	buildMap: function (source, include, exclude) {
		return _.map(_.difference(_.union(source, include), exclude), function (file) {
			return new SpriteImage(file, this);
		}, this);
	},

	/**
	 * Gets list of files grouped by sprite map, where each sprite map files list
	 * consists of files from source directory including resources provided by
	 * include option and without resources defined in exclude option.
	 * When merge option si set to true, only one sprite map set will be returned.
	 * Otherwise number of sprite maps depends on number of non-empty subdirectories
	 * inside source directory.
	 * @param fn {Function} Callback
	 */
	getImages: function (fn) {
		async.parallel([
			this.getSourceFiles.bind(this),
			this.getIncludeFiles.bind(this),
			this.getExcludeFiles.bind(this)
		], function (err, results) {
			if (err) {
				return fn(err);
			}
			var resp = [], maps, map;
			if (this.options.merge) {
				maps = this.splitIntoMaps(results[0]);
//				console.log(results[0],maps);
				resp.push({
					name: this.name,
					images: this.buildMap.apply(this, results)
				});
			} else {
				maps = this.splitIntoMaps(results[0]);
//				console.log(results[0],maps);
				resp = _.map(maps, function (mapFiles, mapName) {
					return {
						name: mapName,
						images: this.buildMap(mapFiles, results[1], results[2])
					};
				}, this);
			}
			fn(null, resp);
		}.bind(this));
	},
	/**
	 * Creates on the fly callback function, which when called
	 * will emit event with the specified @event and call @fn function
	 * when provided. Function will be called in SpriteMap instance context.
	 * Both emitter and function will pass all arguments from the callback.
	 * @param event {String} Name of the emited event.
	 * @param [fn] {Function} Optional callback function.
	 * @returns {Function}
	 */
	emitter: function (event, fn) {
		return function () {
			var args = _.toArray(arguments);
			if (_.isFunction(fn)) {
				fn.apply(this, args);
			}
			this._emitterData[event] = args;
			this.emit.apply(this, _.flatten([event, args]));
		}.bind(this);
	},
	done: function(event, fn) {
		if (this._emitterData[event]) {
			fn.apply(this, this._emitterData[event]);
		} else {
			this.on(event, fn);
		}
		return this;
	},
	build: function (fn) {
        var self = this;
        this.getImages(function (err, maps) {
            if (err) {
                return fn(err);
            }
            async.map(maps, function (map, next) {
                async.map(map.images, function (image, next) {
                    image.size(next);
                }, function (err, result) {
                    if (err) {
                        return next(err);
                    }
                    map.images = result;
                    map.layout = self.layout(result);
                    next(null, map);
                });
            }, self.emitter('build', fn));
        });
        return this;
	},
	create: function (fn) {
		return this.build(function (err, maps) {
			async.each(maps, function (map, next) {
				async.applyEach(this.writers, map, next);
			}.bind(this), this.emitter('create', fn));
		}.bind(this));
	},
	/**
	 * Utility function to build full path to the build file,
	 * ex. PNG, HTML, CSS, etc.
	 */
    getFileName: function (name, extension) {
        return path.join(this.dest, name + '.' + extension);
    }
});

module.exports = exports = SpriteMap;
