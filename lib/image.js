var gm = require('gm');
var path = require('path');
var _ = require('lodash');

function SpriteImage(file, config) {
	this.merged = config.options.merge;
	this.baseDir = config.src;

	this.file = file;
	this.x = 0;
	this.y = 0;
	this.width = 0;
	this.height = 0;

	this.name = this.getSpriteName();
}

SpriteImage.prototype = {
	/**
	 * Calls GraphicsMagic method to tetect sprite image width and height.
	 * If method was called before, stored values.
	 * @async
	 */
	size: function (fn) {
		if (this.width && this.height) {
			return fn(null, this);
		}
		gm(this.file).size(function (err, data) {
			if (err) {
				return fn(err);
			}
			_.extend(this, data);
			fn(null, this);
		}.bind(this));
	},
	/**
	 * Generates sprite name.
	 * @returns {String} Sprite name.
	 */
	getSpriteName: function () {
		var fileExt = path.extname(this.file);
		if (this.merged) {
			return this.file.replace(this.baseDir, '').replace(fileExt, '').replace(/[\\/]/g, '-').replace(/^\-/,'');
		} else {
			return path.basename(this.file, fileExt);
		}
	}
};

module.exports = SpriteImage;
