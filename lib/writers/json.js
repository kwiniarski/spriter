var fs = require('fs');

module.exports = exports = function jsonWriter(file, data, fn) {
	var json = JSON.stringify(data, null, "\t");
	fs.writeFile(file, json, function(err) {
		fn(err, json);
	});
};
