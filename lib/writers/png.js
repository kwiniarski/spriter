var gm = require('gm');
//var _ = require('lodash');

module.exports = function pngWriter(file, data, fn) {

//	var start = _.now();
	var layout = data.layout;
//	var file = this.getFileName(map.name, 'png');
	var image = gm(layout.width, layout.height, layout.color);
//	var callback = this.emitter('png', fn);

	layout.commands.forEach(function (cmd) {
		image[cmd[0]].apply(image, cmd[1]);
	});

	image.noProfile().write(file, function (err) {
		if (err) {
			return fn(new Error('Unable to write file "' + file + '".'));
		}
		gm(file).identify(function (err, info) {
//			console.log(err, info);
//                    console.log('[INFO] Sprite map "%s" created in %s', file, _.timeElapsedFrom(start));
//                    ['Geometry', 'Format', 'Compression', 'Filesize'].forEach(function(property){
//                        console.log('       %s: %s', property, info[property]);
//                    });
			fn(null, info);
		});
	});
};
