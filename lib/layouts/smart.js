var _ = require('lodash');

module.exports = function smartLayout(files) {
	var map = {
		commands: [],
		width: 0,
		height: 0,
		color: 'transparent',
		waste: 0
	};
	var totalArea = _.reduce(files, function (area, file) {
		return area + (file.width * file.height);
	}, 0);
	var maxWidth = Math.sqrt(totalArea);
	var currentX = 0;
	var currentY = 0;

	// Sort sprites before applying them to the map.
	// We sort from the highest to the lowest.
	// Calculate position of each sprite
	files.sort(function (a, b) {
		var diff = b.height - a.height;
		if (!diff) {
			diff = b.width - a.width;
		}
		return diff;
	}).forEach(function (file) {
		file.x = currentX * -1;
		file.y = currentY * -1;

		currentX += file.width;

		// Extend image height
		if (map.height < currentY + file.height) {
			map.height = currentY + file.height;
		}

		// Extend image width
		if (map.width < currentX) {
			map.width = currentX;
		}

		// Go to the next row
		if (currentX > maxWidth) {
			currentX = 0;
			currentY = map.height;
		}

		// TODO, BUG?
		// Some images after applying to the map have their background bigger
		// for one pixel. Seems to be a bug in GM. Executing GM "draw image" command
		// in the order in which sprites are sorted, guarantees they are placed
		// from the top left to the bottom right. This reduces impact of this bug.
		map.commands.push(['draw', [
			"image",
			'Over',
			Math.abs(file.x) + ',' + Math.abs(file.y),
			file.width + ',' + file.height,
			file.file
		]]);
	});

	map.waste = (100 * (1 - (totalArea / (map.width * map.height)))).toFixed(2);

	return map;
};
