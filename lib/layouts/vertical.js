module.exports = function verticalLayout(files) {
	var map = {
			script: {
				commands: [],
				createWidth: 0,
				createHeight: 0,
				createColor: 'transparent'
			},
			sprites: {}
		},

		// Used to calculate total waste ratio (empty and unused space on the sprite map
		// which could be possibly filled by sprites).
		totalArea = 0,

		width = 0,
		height = 0,
		file, i;

	for (i in files) {
		file = files[i];

		file.top = height * -1;

		totalArea += file.width * file.height;
		height += file.height;
		if (width < file.width) {
			width = file.width;
		}

		// Add sprite to the info object
		map.sprites[file.sprite] = file;
		map.script.commands.push(['draw', [
			"image",
			'Over',
			Math.abs(file.left) + ',' + Math.abs(file.top),
			file.width + ',' + file.height,
			file.filePath
		]]);
	}

	// Update GM script
	map.script.createWidth = width;
	map.script.createHeight = height;
	map.waste = (100 * (1 - (totalArea / (width * height)))).toFixed(2);

	// How much space is wasted?
	console.log('[DEBUG] Vertical layout wasted space: %s%.', map.waste);

	return map;
};
