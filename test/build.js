var fst = require("fs-tools");
var gm = require('gm');
var path = require('path');
var _ = require('lodash');
var tmpdir = fst.tmpdir();

//console.log('Build will create files in "%s" directory.', tmpdir);

// Set temp dir name, and create it before tests
//beforeEach(function () {
//	fst.mkdirSync(tmpdir, '0777');
//});
//
//// Before each test recreate tmp dir
////beforeEach(function () {
////	fst.removeSync(tmpdir);
////	fst.mkdirSync(tmpdir, '0777');
////});
//
//// Remove temp dir after tests
//afterEach(function (done) {
////	setTimeout(function () {
//		fst.remove(tmpdir, done);
////	}, 250);
//});

function createSprites(sprites) {
    function random(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function randomColor() {
        var hex = '0123456789abcdef',
            len = hex.length - 1,
            color = '#';
        while(color.length < 7) {
            color += hex[random(0, len)];
        }
        return color;
    }

    function randomExt() {
        return ['.png', '.gif'][random(0,1)];
    }

    function createSprite() {
        var width = random(5,50);
        var height = random(5,50);
        var color = randomColor();
        var dir = path.join(__dirname, '..', '..', 'test', 'sprites');
        var name = path.join(dir, 'sprite_' + width + 'x' + height + color.replace('#','_') + randomExt());

    //	util.mkdir(dir);
        gm(width, height, color).write(name, function(err){
            if (err) {
                return console.error(err);
            }
            console.log('Created: %s.', name);
        });
    }

    if (!sprites) {
        sprites = 1;
    }
	while (sprites--) {
		createSprite();
	}
}

module.exports = exports = {
	tmpdir: tmpdir,
    createSprites: createSprites,
	createTmpDir: function () {
		fst.mkdirSync(tmpdir, '0777');
	},
	removeTmpDir: function (done) {
		setTimeout(function () {
			fst.remove(tmpdir, done);
		}, 300);
	}
};





