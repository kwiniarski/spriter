var SpriteMap = require('../../lib/map.js');
var SpriteImage = require('../../lib/image.js');
var assert = require("assert");
var path = require("path");
var fs = require("fs");
var build = require("../build.js");


SpriteMap.prototype._defaults.src = './test/fixtures';
SpriteMap.prototype._defaults.dest = build.tmpdir;
SpriteMap.prototype._defaults.output = Object.keys(SpriteMap.prototype._writers);


describe('SpriteMap', function () {

	describe('@param name', function () {

		it('should be required', function () {
			assert.throws(function () {
				var config = new SpriteMap();
			});
		});

		it('should start with a letter', function () {
			assert.throws(function () {
				var config = new SpriteMap('.bad-name');
			});
			assert.throws(function () {
				var config = new SpriteMap('0bad-name');
			});
			assert.throws(function () {
				var config = new SpriteMap('-bad-name');
			});
			assert.throws(function () {
				var config = new SpriteMap('_bad-name');
			});

			var config1 = new SpriteMap('good-name'),
				config2 = new SpriteMap('Good-Name');

			assert.equal(config1.name, 'good-name');
			assert.equal(config2.name, 'Good-Name');
		});

		it('should contain only az, AZ, 09, _ & - characters', function () {
			var config = new SpriteMap('good-NAME_123');
			assert.equal(config.name, 'good-NAME_123');
		});

		it('should not contain special characters', function () {
			'~`!@#$%^&*()_+-={}[]:"|;\'\\<>?,./ '.split().forEach(function (char) {
				assert.throws(function () {
					var config = new SpriteMap('bad-name' + char);
				});
			});
		});
	});

	describe('@param options', function () {

		it('should be optional', function () {
			assert.doesNotThrow(function () {
				new SpriteMap('test');
			});
		});

		it('should be equal to defaults when empty', function () {
			var config = new SpriteMap('test');
			assert.deepEqual(config.options, SpriteMap.prototype._defaults);
		});

		it('should be an object when provided', function () {
			assert.throws(function () {
				new SpriteMap('test', []);
			});
		});

	});

	describe('#src & #dest properties', function () {
		it('should be a valid path when relative path was provided', function () {
			var config = new SpriteMap('test', {
				src: './test',
				dest: './test'
			});
			assert.ok(typeof config.src === 'string');
			assert.ok(typeof config.dest === 'string');
			assert.equal(config.src, path.join(process.cwd(), './test'));
			assert.equal(config.dest, path.join(process.cwd(), './test'));
		});
		it('should be a valid path when absolute path was provided', function () {
			var config = new SpriteMap('test', {
				src: build.tmpdir,
				dest: build.tmpdir
			});
			assert.ok(typeof config.src === 'string');
			assert.ok(typeof config.dest === 'string');
			assert.equal(config.src, build.tmpdir);
			assert.equal(config.dest, build.tmpdir);
		});
	});

	describe('#include & #exclude property', function () {
		it('should be changed to an Array when provided as a String', function () {
			var config = new SpriteMap('test', {
				include: '.',
				exclude: '.'
			});
			assert.deepEqual(config.include, [process.cwd()]);
			assert.deepEqual(config.exclude, [process.cwd()]);
		});
		it('should have all paths resolved', function () {
			var config = new SpriteMap('test', {
				include: ['.', './test', '../', build.tmpdir],
				exclude: ['.', './test', '../', build.tmpdir]
			});
			assert.deepEqual(config.include, [
				process.cwd(),
				path.resolve(process.cwd(), './test'),
				path.resolve(process.cwd(), '../'),
				path.resolve(build.tmpdir, build.tmpdir)
			]);
			assert.deepEqual(config.exclude, [
				process.cwd(),
				path.resolve(process.cwd(), './test'),
				path.resolve(process.cwd(), '../'),
				path.resolve(build.tmpdir, build.tmpdir)
			]);
		});
	});


	describe('#getIncludeFiles method', function () {
		var config = new SpriteMap('test', {
			include: [
				'./test/fixtures/ui16',
				'./test/fixtures/ui32/airplane-2.png'
			]
		});
		it('should return 3 files', function (done) {
			config.getIncludeFiles(function (err, res) {
				assert.equal(res.length, 3);
				done(err);
			});
		});
	});

	describe('#getExcludeFiles method', function () {
		var config = new SpriteMap('test', {
			exclude: [
				'./test/fixtures/ui16',
				'./test/fixtures/ui32/airplane-2.png'
			]
		});
		it('should return 3 files', function (done) {
			config.getExcludeFiles(function (err, res) {
				assert.equal(res.length, 3);
				done(err);
			});
		});
	});

	describe('#getImages method with @options.merge == true', function () {
		var config = new SpriteMap('test', {
			src: './test/fixtures/ui16',
			include: './test/fixtures/ui32',
			exclude: './test/fixtures/ui32/airplane-2.png'
		});
		it('should be an array with only one element', function (done) {
			config.getImages(function (err, res) {
				assert.equal(res.length, 1);
				assert.equal(res[0].name, 'test');
				done(err);
			});
		});
		it('should return files from src & include without exclude', function (done) {
			config.getImages(function (err, res) {
				assert.equal(res[0].images.length, 3);
				done(err);
			});
		});
		it('should convert each result to SpriteImage object', function (done) {
			config.getImages(function (err, res) {
				if (err) {
					return done(err);
				}

				res[0].images.forEach(function (image) {
					assert.ok(image instanceof SpriteImage);
				});

				done();
			});
		});
	});

	describe('#getImages method with @options.merge == false', function () {
		var config = new SpriteMap('test', {
			include: './test/fixtures/dummy.png',
			exclude: './test/fixtures/ui32/airplane-2.png',
			merge: false
		});
		console.log(config.src);
		it('should return 3 maps', function (done) {
			config.getImages(function (err, res) {
				assert.equal(res.length, 3);
				done(err);
			});
		});
		it('should return 3 images in map "test"', function (done) {
			config.getImages(function (err, res) {
				assert.equal(res[0].name, 'test');
				assert.equal(res[0].images.length, 3);
				done(err);
			});
		});
		it('should return 3 images in map "test-ui16"', function (done) {
			config.getImages(function (err, res) {
				assert.equal(res[1].name, 'test-ui16');
				assert.equal(res[1].images.length, 3);
				done(err);
			});
		});
		it('should return 2 images in map "test-ui32"', function (done) {
			config.getImages(function (err, res) {
				assert.equal(res[2].name, 'test-ui32');
				assert.equal(res[2].images.length, 2);
				done(err);
			});
		});
	});

//	describe('#config property', function () {
//		it('should be an instance of SpriteConfig', function () {
//			var map = new SpriteMap('test');
//			assert.ok(map.config instanceof SpriteConfig);
//		});
//	});
	describe('#build method', function() {
		var map = new SpriteMap('test', {
			exclude: './test/fixtures/ui32/airplane-2.png',
			merge: false
		});
		it('should create SpriteImage objects in each map object', function (done) {
			map.build(function(err, data){
				if (err) {
					return done(err);
				}
				data.forEach(function (map) {
					map.images.forEach(function (image) {
						assert.ok(image instanceof SpriteImage);
					});
				});
				done();
			});
		});
		it('should set width and height on sprite images', function (done) {
			map.build(function(err, data){
				if (err) {
					return done(err);
				}				
				data.forEach(function (map) {
					map.images.forEach(function (image) {
						assert.ok(image.width > 0);
						assert.ok(image.height > 0);
					});
				});
				done();
			});			
		});
		it('should set x,y coordinates on sprite images', function (done) {
			map.build(function(err, data){
				if (err) {
					return done(err);
				}				
				data.forEach(function (map) {
					map.images.forEach(function (image, i) {
						if (i === 0) {
							assert.ok(image.x === 0 && image.y === 0);
						} else {
							assert.ok(image.x !== 0 || image.y !== 0);
						}
					});
				});
				done();
			});
		});
		it('should create GM build script', function (done) {
			map.build(function(err, data){
				if (err) {
					return done(err);
				}				
				data.forEach(function (map) {
					assert.ok(map.layout);
					assert.ok(map.layout.commands, 'Missing commands array');
					assert.ok(map.layout.width > 0, 'Map width should be greater then 0');
					assert.ok(map.layout.height > 0, 'Map height should be greater then 0');
				});
				done();
			});			
		});
		it('should fail when at least one image cannot be read', function (done) {
			var map = new SpriteMap('test', {
//				src: './test/fixtures',
				include: './test/fixtures/dummy.png' // Nonexisting image
			});
			map.build(function (err, data) {
				assert.ok(err);
				done();
			});
		});
	});
//	describe.skip('#metadata method', function() {
//		it('should throw Error when empty');
//		it('should return JSON object with sprite map metadata');
//	});
	describe('#write method with #config.merge = true', function () {

		beforeEach(build.createTmpDir);
		afterEach(build.removeTmpDir);

		var spritemap = new SpriteMap('test', {
			merge: true
		});

		it('should create files from #config.output in #config.src', function (done) {
			spritemap.create(function (err) {
                if (err) {
                    return done(err);
                }
				this.on('create', function(err){
					spritemap.options.output.forEach(function (writer) {
						var file = spritemap.getFileName('test', writer);
						assert.ok(fs.existsSync(file), 'Missing file: ' + file);
					});
					done(err);
				});
			});
		});
	});
	describe('#write method with #config.merge = false', function () {

		beforeEach(build.createTmpDir);
		afterEach(build.removeTmpDir);

		var spritemap = new SpriteMap('test', {
			merge: false
		});

		it('should create files from #config.output in #config.src', function (done) {
			spritemap.create(function (err) {
                if (err) {
                    return done(err);
                }
				this.on('create', function(err){
					spritemap.options.output.forEach(function (writer) {
						var file = spritemap.getFileName('test', writer);
						assert.ok(fs.existsSync(file), 'Missing file: ' + file);
					});
					done(err);
				});
			});
		});
	});
	describe('event emitter', function () {

		beforeEach(build.createTmpDir);
		afterEach(build.removeTmpDir);

		function spritemap() {
			return new SpriteMap('test', {
				src: './test/fixtures',
				dest: build.tmpdir,
				output: ['css','json','html','png', 'less']
			});
		}
		function create() {
			return spritemap().create();
		}

		it('should emit "build" event', function (done) {
			create().done('build', done);
		});
        it('should emit "create" event', function (done) {
			create().done('create', done).done('html', function(){
//				console.log(arguments);
			});
		});
		it('should emit "png" event', function (done) {
			create().done('png', done);
		});
		it('should emit "css" event', function (done) {
			create().done('css', done);
		});
		it('should emit "less" event', function (done) {
			create().done('less', done);
		});
		it('should emit "html" event', function (done) {
			create().done('html', done);
		});
		it('should emit "json" event', function (done) {
			create().done('json', done);
		});
		it('should work when event is added after #create() has beed completed', function (done) {
			var sprite = spritemap();
			sprite.create(function (err) {
				if (err) {
					return done(err);
				}
				setTimeout(function () {
					sprite.done('create', done);
				}, 500);
			});
		});
	});
});
