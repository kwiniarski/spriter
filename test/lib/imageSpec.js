var SpriteMap = require('../../lib/map.js');
var SpriteImage = require('../../lib/image.js');
var assert = require("assert");
var path = require("path");
var build = require("../build.js");
var _ = require("lodash");

describe('SpriteImage', function () {
	
//	var buildDir;
//	
//	before(function () {
//		buildDir = fst.tmpdir();
//		fst.mkdirSync(buildDir, '0777');
//	});
	
	
	describe('#name property', function () {
		
//		beforeEach(function () {
//			fst.removeSync(buildDir);
//			fst.mkdirSync(buildDir, '0777');
//		});
		
		it('should be build from file name and parent directories names when merge=true', function(done) {
			var config = SpriteMap('test', {
				src: './test/fixtures',
				dest: build.tmpdir,
				merge: true
			});			
			config.getImages(function(err, maps){
				if (err) {
					return done(err);
				}
				var sprites = _.pluck(maps[0].images, 'name');
                
				assert.equal(maps[0].name, 'test');
				assert.equal(sprites.length, _.uniq(sprites).length);
                
				done();
			});
		});
		it('should be build only from file name when merge=false', function (done) {
			var config = SpriteMap('test', {
				src: './test/fixtures',
				dest: build.tmpdir,
				merge: false
			});			
			config.getImages(function(err, maps){
				if (err) {
					return done(err);
				}
				var sheets  = _.pluck(maps, 'name');
				var images  = _.flatten(_.pluck(maps, 'images'));
				var names   = _.pluck(images, 'name');
                
				// There should be no duplicated sprite map's names
				assert.equal(sheets.length, _.uniq(sheets).length);
				assert.equal(names.length, 6);
				assert.equal(_.uniq(names).length, 2);
                
				done();
			});
			
		});
	});
});
