# Spriter

**Spriter** is a tool for creating sprite maps in Node.js. It consists from Node.js module, command line interface and tasks for Grunt and Gulp.

---

## Installation

Please see requirements section before installing and using **Spriter**.

```
npm install spriter
```

## Usage
See Examples section to see how to use Spriter as Node.js module, command line tool or Grunt/Gulp task.

### Options


#### src
Directory in witch sprites are located.
#### dest
Directory where sprite map will be created.
#### include
Additional directories or files to include. If you are using `merge` option set to true, then additional files will be included in each sprite map.
#### exclude
Additional directories or files to exclude. If you are using `merge` option set to true, then additional files will be excluded from each sprite map.
#### merge
Can be `true` or `false` (default). When option is set to `false` all files from `src` directory and it's subdirectories will be stored in one sprite map. If this option is set to `false`, then separate sprite map will be created for each directory.
#### output

### Examples

#### Node.js module
Node.js module is the most basic usage form of Spriter. All other usage examples are using described below Node.js module under the hood. While using Spriter as your application submodule you have and access to the module's prototype chain. Output data can be retrived through the callback functions or events, as Spriter is an instance of `EventEmitter`. Data retrived through callbacks or events can be a `String` or `Stream`.

```javascript
var spriter = require('spriter');
var spritemap = spriter.SpriteMap('icons');

// using callback
spritemap.create(function (data) {
});

// using event
// note, we are using custom 'done' method instead of EventEmitter's 'on'
spritemap.done('create', function(data) {
});
```

#### Command line
#### Grunt task
#### Gulp task

## Requirements

Spriter requires GraphicsMagic (GM) to be installed on your system. Please visit GM website to see how to properly install GM binary on Windows, MacOS or Linux.

## Changelog

## Known issues

## Road map
- [ ] Add margin to sprite images


## License
Copyright &copy; 2014, Krzysztof Winiarski. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
